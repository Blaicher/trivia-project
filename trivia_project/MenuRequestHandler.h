#pragma once
#include "IRequestHandler.h"
#include "SqliteDataBase.h"
#include "json.hpp"
#include "Room.h"
#include "RoomManager.h"
#include "RequestHandlerFactory.h"
#include "StatisticsManager.h"
#define PATH_DB_DEFAULUT "trivia_data_base.sqlite"
using json = nlohmann::json;
class RequestHandlerFactory;
class MenuRequestHandler:public IRequestHandler
{
public:
	MenuRequestHandler(RequestHandlerFactory* handle);
	~MenuRequestHandler();
	bool isRequestRelevent(RequestInfo info);
	RequestResult handleRequest(RequestInfo info);
private:
	//Private functions
	RequestResult signout(RequestInfo requestInfo);
	RequestResult getRooms(RequestInfo requestInfo);
	RequestResult getPlayersInRoom(RequestInfo requestInfo);
	RequestResult getPersonalStats(RequestInfo requestInfo);
	RequestResult getHighScore(RequestInfo requestInfo);
	RequestResult joinRoom(RequestInfo requestInfo);
	RequestResult createRoom(RequestInfo requestInfo);
	//Private variables
	RequestHandlerFactory* m_handlerFactor;
};


