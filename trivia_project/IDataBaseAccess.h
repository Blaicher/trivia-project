#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include "sqlite3.h"
#include <unordered_map>
using std::string;
class IDataBaseAccess
{
public:
    virtual std::unordered_map<std::string, std::string>getUsers()=0;
    virtual int getNumPlayers()=0;
    virtual bool logout(string username)=0;
    virtual std::vector<std::string> getHighScore()=0;
    virtual void createDB_Questions()=0;
    virtual void addQuestionsToDataBase()=0;
    virtual void initilizeDB()=0;
    virtual void LoadUsers()=0;
    virtual void LoadStatistics()=0;
    virtual float getPlayerAverageAnswerTime(string username) const=0;
    virtual int getNumOfCorrectAnswers(string username) const=0;
    virtual int getNumOfTotalAnswers(string username) const=0;
    virtual int getNumOfPlayerGames(string username) const=0;
    virtual bool checkPassword(const std::string username, std::string password) const=0;
    virtual bool checkExistingUser(const std::string username) const=0; // Function to check if a user exists in the database
    virtual bool signUp(std::string username, std::string password, std::string email)=0;

};