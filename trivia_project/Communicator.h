#pragma comment (lib, "ws2_32.lib")
#pragma once
#include <WinSock2.h>
#include <Windows.h>
#include <thread>
#include <queue>
#include <map>
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#define SERVER_PORT 9999
class Communicator
{
public:
	Communicator();
	Communicator(RequestHandlerFactory* factory);
	~Communicator();
	void bindAndListen(int port);
	void startHandleRequests();
	static std::map<string, SOCKET> connectedUSers;
	unsigned int getLen(char* buffer) const;
	static void sendToMultipleUsers(std::vector<string>usernames,const char*buffer);
private:
	void insertConnectedUser(SOCKET client_socket);
	RequestHandlerFactory* m_factory;
	void handleNewClient(SOCKET client_socket);
	std::map<SOCKET, IRequestHandler*> m_clients;
	SOCKET m_serverSocket;
};