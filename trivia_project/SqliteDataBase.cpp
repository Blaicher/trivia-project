#include "SqliteDataBase.h"
#include "sqlite3.h"
#include "IDataBaseAccess.h"
#include <iostream>
#include "json.hpp"
#include "Request.h"
#include<fstream>
#define NUM_SCORES 5
#define ARGUMENTS 5
#define PASSWORD 1
#define EMAIL 2
#define NUM_QUESTIONS 10
#define NUM_INCORRECT_ANSWERS 3
//Some global needed functions:
bool comp(std::pair<string, int> a, std::pair<string, int> b);
enum Positions { USERNAME, NUM_OF_CORRECT_ANSWERS, NUM_OF_TOTAL_ANSWERS, NUM_OF_GAMES, AVERAGE_ANSWER_TIME };
//The vector conatains the user names and their passwords, first user name and then password.
/*
The function returns the vector of string pairs to the client.
input: none.
output: the vector of string pairs.
*/
std::unordered_map<std::string, std::string> SqliteDataBase::getUsers()
{
    return this->_Users;
}
/*
The function creates the table in the database of the questions.
input: none.
output: none.
*/
int SqliteDataBase::getNumPlayers()
{
    return this->_Users.size();
}
/*
The function of loging out a spesific user name.
input: user name.
output: true if succeded.
*/
bool SqliteDataBase::logout(string username)
{
    bool foundWantedPair = false; //The varriable has the value of finding the pair we wanted or didn't find it.
    std::unordered_map<std::string, std::string>::iterator it; //The iterator of users.
    it = this->_Users.begin(); //Getting the starting of the unordered map.
    for (; it != this->_Users.end() && !foundWantedPair; it++)
    {
        if ((*it).first == username)
        {
            this->_Users.erase(it); //Deleting the wanted user name.
            foundWantedPair = true; 
        }
    }
    //Checking if the pair is even found.
    if (foundWantedPair == false)
    {
        return false;
    }
    //In case we did find the pair.
    string deletingQuery = "DELETE FROM USERS WHERE Username = '" + username + "';"; //Deleting the unneeded row.
    const char* sqlStatement = deletingQuery.c_str(); //Getting the const char type of data.
    char** errMessage = nullptr;
    int res = sqlite3_exec(this->_Database, sqlStatement, nullptr, nullptr, errMessage); //Running the quary.
    if (res != SQLITE_OK)
    {
        std::cerr << "There was an error!!!";
        return false;
    }
    return true;
}

std::vector<std::string> callBackVector;
/*
The function adds a user to the vector.
input: the data and the columns names and all things about the db.
output: 0.
*/
static int callback(void* (data), int argc, char** argv, char** azColName) {
    if ((int)data == 1)
    {
        // Statistics
        for (int i = 0; i < argc - 1; i++)
        {
            callBackVector.push_back(argv[i]);
        }
    }
    else
    {
        // First of all checking that the name and the password aren't null.
        if (argv[USERNAME] == nullptr || argv[PASSWORD] == nullptr)
        {
            exit(1);
        }
        // Keeping them in variables and adding to the vector.
        std::string username = argv[USERNAME];
        std::string password = argv[PASSWORD];
        callBackVector.push_back(username);
        callBackVector.push_back(password);
    }
    return 0;
}
/*
Function to use in order to sort an unordered map by the scores.
input: the two pairs to sort.
output: true if the second(score) of the b is bigger than the a's second.
*/
bool comp(std::pair<string, int> a, std::pair<string, int> b)
{
    return a.second < b.second;
}
/*
The function returns the highest scores in the game.
input: none.
output: the highest scores in the game.
*/
std::vector<std::string> SqliteDataBase::getHighScore()
{
    std::vector<string> ansHighScore; //The answer of the high score.
    std::unordered_map<std::string, User>::iterator it; //The iterator passing the unordered map.
    it = this->_Statistics.begin(); //Getting the start of the stats.
    std::unordered_map<std::string, float>scores; //The map of the score of each player.
    for (; it != this->_Statistics.end(); it++)
    {
        string name = (*it).first; //Getting the name of the current user. 
        float score = (*it).second.numOfCorrectAnswers / (*it).second.averageAnswerTime;  //Getting the score by its calculation.
        std::pair<string, float>pairCurrScore(name, score); //Creating the pair.
        scores[pairCurrScore.first] = pairCurrScore.second; //Adding to the scores map.
    }
    std::vector<std::pair<string, int>> elems(scores.begin(), scores.end()); //Converting the unordered map to a vector.
    std::sort(elems.begin(), elems.end(), comp); //Sorting the vecto
    int i = 0; //Loup varriable.
    //In this loup we get the 5 highest scores.
    for (i = 0; i < NUM_SCORES; i++)
    {
        ansHighScore.push_back(elems[i].first);
    }
    return ansHighScore;
}
/*
The function creates the table in the database of the questions.
input: none.
output: none.
*/
void SqliteDataBase::createDB_Questions()
{
    const char* sqlStatement = "CREATE TABLE QUESTIONS(Category TEXT NOT NULL,Type TEXT NOT NULL, Difficulty TEXT NOT NULL,Question TEXT NOT NULL,Corrent_Answer TEXT NOT NULL,Incorrect_Answers TEXT NOT NULL)";
    char** errMessage = nullptr;
    int res = sqlite3_exec(this->_Database, sqlStatement, nullptr, nullptr, errMessage); //Running the quary.
    if (res != SQLITE_OK)
    {
        std::cerr << "There was an error!!!";
        return;
    }
}
/*
The function adds questions to the database.
input: none.
output: none.
*/
void SqliteDataBase::addQuestionsToDataBase()
{
    std::ifstream questionsFile("questions.json", std::ifstream::binary);
    json questions; //The object of the questions itself.
    questionsFile >> questions; //Getting the data into the object.
    json* arrQuestions; //This is the array of the questions.
    arrQuestions = new json[NUM_QUESTIONS]; //Defining the memory of the questions.
    int i = 0; //Loup varriable.
    //The loup of adding the questions to the data base.
    for (i = 0; i < NUM_QUESTIONS; i++)
    {
        string queryInserting = "INSERT INTO QUESTIONS VALUES(" + arrQuestions[i]["category"] + arrQuestions[i]["type"] + arrQuestions[i]["difficulty"] + arrQuestions[i]["question"] + arrQuestions[i]["correct_answer"] + arrQuestions[i]["incorrect_answers"];
        queryInserting += ");"; //Adding to the string.
        const char* sqlStatement = queryInserting.c_str(); //Getting the query.
        char** errMessage = nullptr;
        int res = sqlite3_exec(this->_Database, sqlStatement, nullptr, nullptr, errMessage); //Running the quary.
        if (res != SQLITE_OK)
        {
            std::cerr << "There was an error!!!";
            return;
        }
    }
}
/*
The function creates the table in the databse in case
that the table was empty in the begining.
input: none.
output: none.
*/
void SqliteDataBase::initilizeDB()
{
    const char* sqlStatement = "CREATE TABLE USERS(Username TEXT NOT NULL,Password TEXT NOT NULL);";
    char** errMessage = nullptr;
    int res = sqlite3_exec(this->_Database, sqlStatement, nullptr, nullptr, errMessage); //Running the quary.
    const char* sqlStatisticsCreation = "CREATE TABLE USER_STATISTICS(Username TEXT NOT NULL, Number_Of_Questions_Answered_Correctly INT NOT NULL, QUESTION_AMOUNT INT NOT NULL, Games_Played INTEGER NOT NULL, Average_Question_Time FLOAT NOT NULL, Games_Won INT NOT NULL);";
    const char* sqlUserStatisticsCreation = "CREATE TABLE STATISTICS(Username TEXT NOT NULL PRIMARY KEY, QUESTIONS_CORRECT INT NOT NULL, QUESTION_AMOUNT INT NOT NULL, POINTS FLOAT NOT NULL, PLACE INT NOT NULL, TIME_FOR_EACH_QUESTION FLOAT NOT NULL);";
    if (res != SQLITE_OK)
    {
        std::cerr << "There was an error!!!";
        return;
    }
    res = sqlite3_exec(this->_Database, sqlStatisticsCreation, nullptr, nullptr, errMessage);
    if (res != SQLITE_OK && sqlite3_exec(this->_Database, sqlUserStatisticsCreation, nullptr, nullptr, errMessage))
    {
        std::cerr << "There was an error!";
        return;
    }
}


/*
The constructor of the class, opens the connection with the database and
loding the users into a map.
input: the path of the database.
output: none.
*/
SqliteDataBase::SqliteDataBase(std::string path) {
    int rc = sqlite3_open(path.c_str(), &_Database); // Opening database, restoring result of action in rc
    if (rc) // Should be 0 if the process is successful
    {
        //In case the table doesn't exist we call the right function.
        this->initilizeDB();
        this->createDB_Questions();
        this->addQuestionsToDataBase();
    }
    // Can assume the database opened successfully
    // Now loading existing data!
    LoadUsers();
    LoadStatistics();
}
SqliteDataBase::~SqliteDataBase()
{
    this->_Database = nullptr;
    sqlite3_close(this->_Database); //Closing the database.
}
/*
 Function will retrieve users' information from database and store it.
 input: none.
 output: none.
 */
void SqliteDataBase::LoadUsers() {
    int rc = sqlite3_exec(_Database, "SELECT * FROM USERS;", callback, (void*)0, &_zErrMsg); //Running the query.
    for (int i = 0; i < callBackVector.size(); i++)
    {
        //Checking when we got to user name field and adding to the map from the vector.
        if (i % 2 != 0) {
            _Users[callBackVector[i - 1]] = callBackVector[i]; //Using the name and then the password.
        }
    }
    callBackVector.clear();

}

void SqliteDataBase::LoadStatistics()
{
    std::string query = "SELECT * FROM USER_STATISTICS;";
    int rc = sqlite3_exec(_Database, query.c_str(), callback, (void*)1, &_zErrMsg);
    int j = 0;
    for (int i = 0; i < callBackVector.size() / 5; i++)
    {
        std::string curUsername;
        User currentUser;
        for (j = 0; j < ARGUMENTS; j++)
        {
            switch (j)
            {
            case USERNAME:
                curUsername = callBackVector[i * ARGUMENTS + j];
                break;
            case NUM_OF_CORRECT_ANSWERS:
                currentUser.numOfCorrectAnswers = std::stoi(callBackVector[i * ARGUMENTS + j]);
                break;
            case NUM_OF_TOTAL_ANSWERS:
                currentUser.numOfTotalAnswers = std::stoi(callBackVector[i * ARGUMENTS + j]);
                break;
            case NUM_OF_GAMES:
                currentUser.numOfPlayerGames = std::stoi(callBackVector[i * ARGUMENTS + j]);
                break;
            case AVERAGE_ANSWER_TIME:
                currentUser.averageAnswerTime = std::stof(callBackVector[i * ARGUMENTS + j]);
                break;
            }
        }
        _Statistics[curUsername] = currentUser;
    }
    callBackVector.clear();
}
/*
The function prints all the users.
input: none.
output: none.
*/
void SqliteDataBase::_PrintUsers() {

    for (auto it = _Users.begin(); it != _Users.end(); it++)
    {
        std::cout << it->first << " - " << it->second << std::endl;
    }

}

/*
The function checks if a spesific user name exists.
input: the user name string to check.
output: true if exists and false if doesn't.
*/
bool SqliteDataBase::checkExistingUser(const std::string username) const
{
    auto it = _Users.find(username); //Looking for the user name.
    if (it != _Users.end())
    {
        return true;
    }
    return false;
}
/*
The function signs up the user.
input: the user name and the password.
output: the status of the process.
*/
bool SqliteDataBase::signUp(std::string username, std::string password, std::string email)
{
    if (checkExistingUser(username)) { // Checking if the username already exists..
        std::cerr << "Failed to sign up" << std::endl;
        return false;
    }
    if (username.compare("") == 0 || password.compare("") == 0) // Cannot be null!
    {
        std::cerr << "Values cannot be null!" << std::endl;
        return false;
    }
    // Signing up
    std::string sql = "INSERT INTO USERS(Username, Password,Email) VALUES(" + username + ", " + password + ", " + email + ");"; //The query itself.
    int result = sqlite3_exec(_Database, sql.c_str(), nullptr, nullptr, &_zErrMsg);
    std::string newUserName = username; //The new user name.
    std::string newPassword = password; //The new password.
    _Users[newUserName] = newPassword; //Adding to the original dynamic database object.
    if (!result)
    {
        std::cerr << "Failed to sign up!" << std::endl;
        return false;
    }
    return true; // Sign up is complete!
}
/*
The function checks if the password matches the user name.
input: the user name and the password.
output: true if the password matches the user name and false if not.
*/
bool SqliteDataBase::checkPassword(const std::string username, std::string password) const
{
    //Checking if the user name exists.
    if (checkExistingUser(username))
    {
        //Checking if the user name matches the password.
        if ((_Users.find(username)->second.compare(password)))
        {
            return true;
        }
    }
    return false;
}

float SqliteDataBase::getPlayerAverageAnswerTime(string username) const
{
    return _Statistics.find(username)->second.averageAnswerTime;
}

int SqliteDataBase::getNumOfCorrectAnswers(string username) const
{
    return _Statistics.find(username)->second.numOfCorrectAnswers;
}

int SqliteDataBase::getNumOfTotalAnswers(string username) const
{
    return _Statistics.find(username)->second.numOfTotalAnswers;
}

int SqliteDataBase::getNumOfPlayerGames(string username) const
{
    return _Statistics.find(username)->second.numOfPlayerGames;
}
