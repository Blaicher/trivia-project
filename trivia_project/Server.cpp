#pragma comment(lib,"ws2_32.lib")
#include "WSAInitializer.h"
#include "Server.h"
#include <string>
#include <iostream>
#include <exception>

/*
The instructor of the class.
input: none.
output: none.
*/
Server::Server()
{
	this->m_database = new SqliteDataBase(PATH_DB_DEFAULUT);
	this->m_factory = new RequestHandlerFactory(m_database);
	this->m_communicator = new Communicator(this->m_factory);
}
/*
The destructor of the class.
input: none.
output: none.
*/
Server::~Server()
{
	delete this->m_communicator;
	delete this->m_database;
	delete this->m_factory;
}
/*
The function that starts to run all the proccess.
input: none.
output: none.
*/
void Server::run()
{
	std::thread t_connector(&Communicator::startHandleRequests, this->m_communicator);
	t_connector.detach();
	std::string curInput;
	try
	{
		while (true)
		{
			std::cin >> curInput;
			if (curInput.compare("Exit"))
			{
				return;
			}
		}
	}
	catch (std::exception& e)
	{
		std::cout << "Error occured: " << e.what() << std::endl;
	}
}
