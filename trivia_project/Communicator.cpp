#include "Communicator.h"
#include <exception>
#include <iostream>
#include <string>
#include <condition_variable>
#include<thread>
#include<string>
#include<iomanip>
#include<chrono>
#include<vector>
#include "LoginRequestHandler.h"
#include "IDataBaseAccess.h"
#include "Response.h"
#include "Request.h"
#include "LoginManager.h"
#define MAX_MESSAGE_SIZE 35
#define MAX_SIZE 2147483647
#define BYTE 1
#define CODE_BYTE_AMOUNT BYTE
#define DATA_LENGTH_BYTE_AMOUNT BYTE * 4
#define STATUS_SUCCESSFULL 1
#define STATUS_UNSUCCESSFULL -1
#define ERROR_CODE 9
#define LOGIN_CODE 2
#define SIGNUP_CODE 1
#define LOGOUT_CODE 3
#define CREATE_ROOM_CODE 4
#define JOIN_ROOM_RESPONSE_CODE 5
#define GET_ROOMS_RESPONSE_CODE 6
#define GET_PLAYERS_IN_ROOM_RESPONSE 7
#define GET_HIGH_SCORE_RESPONSE 8
#define GET_HIGH_SCORE_CODE 11
#define GET_ROOM_CODE 13
#define GET_PERSONAL_STATS_CODE 14

std::map<string, SOCKET> Communicator::connectedUSers;


//Different functions we use in this class.
std::vector<unsigned char> ConvertFromCharBufferToUnsignedCharBuffer(char* buffer,int length);
using std::cout;
/*
The function is the constructor of the class.
input: none.
output: none.
*/
Communicator::Communicator()
{
	// While user is not disconnected (bytes received > 0)
	// Read header 
}
Communicator::Communicator(RequestHandlerFactory* factory)
{
	this->m_factory = factory;
}
/*
The function is the destructor of the class.
input: none.
output: none.
*/
Communicator::~Communicator()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(m_serverSocket);
	}
	catch (...) {}
}
/*
The function takes care of one client.
input: none.
output: none.
*/
void Communicator::handleNewClient(SOCKET client_socket)
{
	RequestResult result;
	RequestInfo information; //The informaation object we are going to return.
	//We recieve information...

	//unsigned char msg;

	char* buffer = (char*)malloc(sizeof(char) * BYTE + 1);
	if (buffer == nullptr)
	{
		return;
	}
	int bytesRead = 1;
	while (true)
	{
		bytesRead = recv(client_socket, buffer, BYTE, 0);
		if (bytesRead > 0)
		{
			std::string Sbuffer = buffer;
			int code = (int)buffer[0];
			printf("%d\n", code);
			buffer = (char*)realloc(buffer, sizeof(char) * DATA_LENGTH_BYTE_AMOUNT + 1);
			if (buffer == nullptr)
			{
				return;
			}
			bytesRead = recv(client_socket, buffer, DATA_LENGTH_BYTE_AMOUNT, 0);

			int len = this->getLen(buffer);
			buffer = (char*)realloc(buffer, sizeof(char) * len + 1);
			if (buffer == nullptr)
			{
				return;
			}
			buffer[len] = '\0';
			bytesRead = recv(client_socket, buffer, len, 0);
			printf("%s", buffer); //Printing the message came from the costumer.
			//Building the request info object to send to the login request handler:
			information.id = code; //Getting the code of the request.
			information.buffer = ConvertFromCharBufferToUnsignedCharBuffer(buffer,len+1); //Getting the information of the request.
			information.receivalTime = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()); //Getting the current time.
			if(this->m_clients[client_socket]->isRequestRelevent(information))
			{
				result = m_clients[client_socket]->handleRequest(information); //Sending to the function.
				//Checking if null ptr is returned.
				delete m_clients[client_socket];
				m_clients[client_socket] = result.newHandler;
				const char* toSend = result.response; //Getting the response from the server.
				send(client_socket, toSend, strlen(toSend), 0); //Sending to the client.
				if (information.id == LOGIN_CODE || information.id == SIGNUP_CODE)
				{
					this->insertConnectedUser(client_socket); //To check if we have users and sockets.
				}
			}
			else
			{
				//Sending an error response.
				ErrorResponse* response = new ErrorResponse;
				response->message = "There was an error!!!";
				const char* toSend = response->message.c_str();
				send(client_socket, toSend, strlen(toSend), 0); //Sending to the client the error response.
				delete response;
			}
			free(buffer);
		}
		else
		{
			free(buffer);
			return;
		}

	}
}

/*
Serving a spesific client.
input: none.
output: none.
*/
void Communicator::bindAndListen(int port)
{
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	m_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (m_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if(::bind(m_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
	{
		throw std::exception(__FUNCTION__ " - bind");
	}
		/*throw std::exception(__FUNCTION__ " - bind");*/

	// Start listening for incoming requests of clients
	if (listen(m_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;


}
/*
The function accepts different things from the socket and acts.
input: none.
output: none.
*/
void Communicator::startHandleRequests()
{
	// notice that we step out to the global namespace
// for the resolution of the function accept

// this accepts the client and create a specific socket from server to this client
	bindAndListen(SERVER_PORT);
	while (true)
	{
		SOCKET client_socket = ::accept(m_serverSocket, NULL, NULL);

		if (client_socket == INVALID_SOCKET)
			throw std::exception(__FUNCTION__);

		// Accepted!
		// the function that handle the conversation with the client
		this->m_clients.insert(std::pair<SOCKET, IRequestHandler*>(client_socket, this->m_factory->createLoginRequestHandler()));
		std::thread curUser(&Communicator::handleNewClient, this, client_socket);
		curUser.detach(); // Detaching so the server does not wait for user to logout
	}
}
/*
The function converts from binary string to a number.
input: the buffer.
output: the number we get.
*/

unsigned int Communicator::getLen(char* buffer) const
{
	unsigned int length = 0;
	for (int i = 0; i < DATA_LENGTH_BYTE_AMOUNT; i++)
	{
		length += buffer[i] * pow(256, DATA_LENGTH_BYTE_AMOUNT - i - 1);
	}
	return length;
}
void Communicator::sendToMultipleUsers(std::vector<string> usernames, const char*buffer)
{
	for (int i = 0; i < usernames.size(); i++)
	{
		//const char* toSend = result.response; //Getting the response from the server.
		send(Communicator::connectedUSers[usernames[i]], buffer, strlen(buffer), 0); //Sen
	}
}
void Communicator::insertConnectedUser(SOCKET client_socket)
{
	std::map<string, SOCKET>::iterator it;
	it = Communicator::connectedUSers.begin();
	for ( ;  it !=  Communicator::connectedUSers.end(); it++)
	{
		if (it->second == -1)
		{
			break;
		}
	}
	if (it != Communicator::connectedUSers.end())
	{
		Communicator::connectedUSers[it->first] = client_socket;
	}
}
/*
The function gets a char* type of object and converts it into a vector of unsigned char*.
input: the char* type of object.
output: vector of unsigned chars.
*/
std::vector<unsigned char> ConvertFromCharBufferToUnsignedCharBuffer(char*buffer,int length)
{
	int i = 0; //Loup varriable.
	std::vector<unsigned char>vectorBuffer; //The varriable we are going to return.
	for (i = 0; i < length; i++)
	{
		vectorBuffer.push_back((unsigned char)buffer[i]); //Pushing into the buffer vector of unsigned chars from the input buffer.
	}
	return vectorBuffer;
}
