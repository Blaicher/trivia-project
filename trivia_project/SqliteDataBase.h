#pragma once
#include<iostream>
#include "sqlite3.h"
#include "IDataBaseAccess.h"
#include<vector>
using std::string;

typedef struct User
{
    float averageAnswerTime;
    int numOfCorrectAnswers;
    int numOfTotalAnswers;
    int numOfPlayerGames;
}User;


class SqliteDataBase : public IDataBaseAccess
{
public:
    std::unordered_map<std::string, std::string>getUsers();
    int getNumPlayers();
    bool logout(string username);
    std::vector<std::string> getHighScore();
    void createDB_Questions();
    void addQuestionsToDataBase();
    void initilizeDB();
    SqliteDataBase(std::string path);
    ~SqliteDataBase();
    void LoadUsers();
    void LoadStatistics();
    float getPlayerAverageAnswerTime(string username) const;
    int getNumOfCorrectAnswers(string username) const;
    int getNumOfTotalAnswers(string username) const;
    int getNumOfPlayerGames(string username) const;
    bool checkPassword(const std::string username, std::string password) const;
    bool checkExistingUser(const std::string username) const; // Function to check if a user exists in the database
    bool signUp(std::string username, std::string password, std::string email);
private:
    sqlite3* _Database;
    char* _zErrMsg = 0;
    std::unordered_map<std::string, std::string> _Users;
    std::unordered_map<std::string, User> _Statistics;
    void _PrintUsers();
};

