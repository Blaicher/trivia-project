#pragma once
#include "SqliteDataBase.h"
#include "LoggedUser.h"
#include<vector>
#include<iostream>
using namespace std;
#define DEFAULT_PATH "trivia_data_base.sqlite"
class LoginManager
{
public:
	LoginManager(IDataBaseAccess* sqlite);
	~LoginManager();
	bool signUp(string name,string password,string email);
	bool login(string name,string password);
	bool logout(string username);
private:
	IDataBaseAccess* m_database;
	vector<LoggedUser*>m_loggedUsers;
};
