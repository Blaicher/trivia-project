#include "RequestHandlerFactory.h"
/*
The constructor of the class.
input: none.
output: none.
*/
RequestHandlerFactory::RequestHandlerFactory(IDataBaseAccess*dataBase)
{  
    this->m_database = dataBase;
    this->loginManager = new LoginManager(dataBase);
    this->m_roomManager = new RoomManager();
    this->m_statisticsManager = new StatisticsManager(this->m_database);
}
/*
The destructor of the class.
input: none.
output: none.
*/
RequestHandlerFactory::~RequestHandlerFactory()
{
    delete this->m_statisticsManager;
    delete this->m_roomManager;
    delete this->loginManager;
}
/*
The function creating the menu request handler appearece.
input: none.
output: new menu request handler object.
*/
MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler()
{
    return new MenuRequestHandler(this);
}
/*
The function logins with the information it gets into the database 
server.
input: user name and password.
output: true if the proccess succeded.
*/
bool RequestHandlerFactory::loginToDB(string username, string password)
{
    bool status = this->loginManager->login(username,password); //Turning to the manager in order to login.
    return status;
}
/*
The function creates a login request handler.
input: none.
output: login request handler pointer.
*/
LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
    LoginRequestHandler* loginReq = new LoginRequestHandler(this); 
    return loginReq;
}
/*
The function returns the login manager of the class.
input: none.
output: the login manager of the class.
*/
LoginManager* RequestHandlerFactory::getLoginManager()
{
    return this->loginManager;
}
/*
The function returns the statistics manager object.
input: none.
output:
*/
StatisticsManager* RequestHandlerFactory::getStatisticsManager()
{
    return this->m_statisticsManager;
}
/*
The function returns the room manager object.
input: none.
output: room manager object.
*/
RoomManager* RequestHandlerFactory::getRoomManager()
{
    return this->m_roomManager;
}
/*
The function logs out a spesific user name.
input: user name.
output: true if succeded and false if not.
*/
bool RequestHandlerFactory::logout(string username)
{
    return this->loginManager->logout(username);
}
/*
Returning the handler of a room admin.
input: none.
output: the object.
*/
RoomAdminRequestHandler* RequestHandlerFactory::createRoomAdminRequestHandler()
{
    RoomAdminRequestHandler* roomAdmin = new RoomAdminRequestHandler(this);
    return roomAdmin;
}
/*
The function creates a room member request handler.
input: none.
output: the room member request handler.
*/
RoomMemberRequestHandler* RequestHandlerFactory::createRoomMemberRequestHandler()
{
    RoomMemberRequestHandler* member = new RoomMemberRequestHandler(this);
    return member;
}
/*
The function signs another user up.
input: the information of the user: name,password and email.
output: true if the proccess succeded and false if not.
*/
bool RequestHandlerFactory::signUpToDB(string username, string password, string email)
{
    bool status = this->loginManager->signUp(username,password,email); //Signing up using the login manager.
    return status;
}

