#include "Room.h"
/*
The constructor of the class.
input: none.
output: none.
*/
Room::Room()
{
}
Room::Room(RoomData roomData)
{
	this->m_metadata = roomData;;
}
/*
The destructor of the class.
input: none.
output: none.
*/
Room::~Room()
{
	delete &this->m_metadata;
	m_users.clear();
}
/*
The function adds the user to the vector.
input: the new user we want to add.
output: none.
*/
void Room::addUser(LoggedUser user)
{
	this->m_users.push_back(user);
}
/*
The function removes the logged user from the vector.
input: the logged user we want to remove.
output: none.
*/
void Room::removeUser(LoggedUser user)
{
	int i = 0; //Loup varriable.
	for (i = 0; i < this->m_users.size(); i++)
	{
		if (user == this->m_users[i])
		{
			this->m_users.erase(this->m_users.begin()+i); //Deleting the correct user.
		}
	}
}
/*
The function returns the names of all users.
input: none.
output: the vector of names.
*/
vector<string> Room::getAllUsers()
{
	vector<string>names; 
	int i = 0; //Loup varriable.
	for ( i = 0; i < this->m_users.size(); i++)
	{
		names.push_back(this->m_users[i].getUsername()); //Adding the name to the vector.
	}
	return names;
}
/*
The function returns the data about the room.
input: none.
output: the data about the room.
*/
RoomData Room::getRoomData()
{
	return this->m_metadata;
}
/*
The function returns the list of logged users in the room.
input: none.
output: the vector of logged users.
*/
std::vector<LoggedUser> Room::getM_user()
{
	return this->m_users;
}
