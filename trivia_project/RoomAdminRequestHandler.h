#pragma once
#include <iostream>
#include<string>
#include "IRequestHandler.h"
#include "Handlers.h"
#include "Room.h"
#include "LoggedUser.h"
#include "RoomManager.h"
#include "RequestHandlerFactory.h"
class RoomAdminRequestHandler :public IRequestHandler
{
public:
	RoomAdminRequestHandler(RequestHandlerFactory* handle);
	bool isRequestRelevent(RequestInfo info);
	RequestResult handleRequest(RequestInfo info);
	RequestResult closeRoom(RequestInfo info);
	RequestResult startGame(RequestInfo info);
	RequestResult getRoomState(RequestInfo info);
private:
	Room* m_room;
	LoggedUser* m_user;
	RoomManager* m_roomManager;
	RequestHandlerFactory* m_handleFactory;
};