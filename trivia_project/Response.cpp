#include "Response.h"
#include <math.h>
#include <algorithm>
#include <stdio.h>
#include <fstream>
#include <iomanip>
#include <sstream>
#define STATUS "status"
#define MESSAGE "message"
#define ERROR_CODE 9
#define LOGIN_CODE 2
#define SIGNUP_CODE 1
#define LEN_OF_DATA 4
#define PAD (char) 0
using std::string;

template<typename ResponseStruct>
const char* CreateMessage(ResponseStruct curStruct);
/*
The function serializes the close room response.
input: close room response struct.
output: the close room response in char*.
*/
const char* JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse response)
{
	return CreateMessage(response);
}
/*
The function serializes the start game response.
input: start game response function.
output: the information in const char*.
*/
const char* JsonResponsePacketSerializer::serializeResponse(StartGameResponse response)
{
	return CreateMessage(response);
}
/*
The function serializes the Get room state response.
input: the get room state response object.
output: the const char* type of object.
*/
const char* JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse response)
{
	unsigned long n = 175;
	//Getting the varriables' values.
	json data; //Getting the data into json type of information.
	data["status"] = response.status;
	data["has_game_begun"] = response.hasGameBegun;
	data["question_count"] = response.questionCount;
	data["answerTimeout"] = response.answerTimeout;
	//Getting all the names into a string.
	string names = "";
	for (int i = 0; i < response.players.size(); i++)
	{
		names += response.players[i];
		names += ",";
	}
	data["players"] = names;
	//Starting to make the returning value itself.
	int length = data.dump().length();
	std::string lengthAsString = std::to_string(length);
	char* msg = (char*)malloc(sizeof(char) * (2 + LEN_OF_DATA + length));
	msg[0] = 10;
	int i = 1;
	for (int i = 1; i < LEN_OF_DATA - lengthAsString.length() + 1; i++)
	{
		msg[i] = (n >> 24 - ((i - 1) * 8)) & 0xFF;
	}

	for (auto chr : lengthAsString)
	{
		msg[i] = (char)((int)chr - '0');
		i++;
	}

	for (auto chr : data.dump())
	{
		msg[i] = (char)((int)chr - '0');
		i++;
	}
	msg[i] = '\0';
	return msg;
}

/*
The function serializes the leave room response object.
input: the leave room response object.
output: the const char* type of information.
*/
const char* JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse response)
{
	return CreateMessage(response);
}

/*
The function serializes the sign up response that came from the server.
input: sign up response object.
output: const char* that describes the sign up response object.
*/
const char* JsonResponsePacketSerializer::serializeResponse(SignupResponse SignupResponse)
{
	return CreateMessage(SignupResponse);
}

/*
The function serializes the error response that came from the server.
input: error response object.
output: const char* that describes the error response object.
*/
const char* JsonResponsePacketSerializer::serializeResponse(ErrorResponse ErrorResponse)
{
	int lengthRes = 0; //This is the length of the response.
	string msga = ErrorResponse.message; //Varriable that represents the message of the error.
	json error; //The json varriable we are going to use.
	error["message"] = msga; //Inserting the message of the error response.
	lengthRes = error.dump().length(); //Getting the length of the response.
	string lengthAsString = std::to_string(lengthRes); //Converting the number to string.
	char* msg = new char[2 + LEN_OF_DATA + lengthRes]; //This is the final message we are going to return. 
	int i = 1;
	int j = 1;
	int c = 0;
	msg[0] = ERROR_CODE;
	std::ostringstream ostr;
	ostr << std::setw(LEN_OF_DATA) << std::setfill('0') << lengthAsString;
	for (c = i; c < (LEN_OF_DATA - lengthAsString.length()); c++);
	{
		msg[c] = '0';
		c++;
	}
	i = c;
	for (auto chr : lengthAsString)
	{
		msg[i] = (char)((int)chr - '0');
		i++;
	}

	for (auto chr : error.dump())
	{
		msg[i] = chr;
		i++;
	}
	msg[i] = '\0';
	printf("%s", msg);
	//Adding the login json to the message.
	return msg; //Returning the content as const char* type of object.
}
/*
The function serializes the login response into const char* type of object.
input: login response type of object.
output: const char* type of object describing the login response.
*/
const char* JsonResponsePacketSerializer::serializeResponse(LoginResponse LoginResponse)
{
	auto msg = CreateMessage(LoginResponse);
	printf("%s", msg);
	//Adding the login json to the message.
	return msg; //Returning the content as const char* type of object.
}


const char* JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse GetRoomResponse)
{
	return CreateMessage(GetRoomResponse);
}
const char* JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse GetPlayersInRoomResponse)
{
	unsigned long n = 175;
	std::string data = "";
	for (int i = 0; i < GetPlayersInRoomResponse.players.size(); i++)
	{
		data += GetPlayersInRoomResponse.players[i];
		data += ",";
	}
	json players;
	players["players"] = data;
	int length = players.dump().length();
	std::string lengthAsString = std::to_string(length);
	char* msg = (char*)malloc(sizeof(char) * (2 + LEN_OF_DATA + length));
	msg[0] = 10;
	int i = 1;
	for (int i = 1; i < LEN_OF_DATA - lengthAsString.length() + 1; i++)
	{
		msg[i] = (n >> 24 - ((i - 1) * 8)) & 0xFF;
	}

	for (auto chr : lengthAsString)
	{
		msg[i] = (char)((int)chr - '0');
		i++;
	}

	for (auto chr : players.dump())
	{
		msg[i] = (char)((int)chr - '0');
		i++;
	}
	msg[i] = '\0';
	return msg;
}
const char* JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse JoinRoomResponse)
{
	return CreateMessage(JoinRoomResponse);
}
const char* JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse CreateRoomResponse)
{
	return CreateMessage(CreateRoomResponse);
}
const char* JsonResponsePacketSerializer::serializeResponse(GetPersonalStatsResponse GetStatisticsResponse)
{
	json info;//The json that represents the information we want to return.
	int j = 0;
	std::vector<std::string>stats = GetStatisticsResponse.statistics; //Getting the object of the statistics.
	string data = ""; //The string that represents the data in the vector.
	unsigned long n = 175;
	for (j = 0; j < stats.size(); j++)
	{
		data += stats[j]; //Adding the current stat from the vector.
		data += ",";
	}
	data[data.length() - 1] = NULL;
	info["status"] = GetStatisticsResponse.status;
	info["statistics"] = data; //Adding data to the information json object.
	int length = info.dump().length();
	std::string lengthAsString = std::to_string(length);
	char* msg = (char*)malloc(2 + LEN_OF_DATA + length);
	int i = 0;
	msg[0] = GetStatisticsResponse.status;
	for (int i = 1; i < (LEN_OF_DATA - lengthAsString.length() + 1); i++)
	{
		msg[i] = (n >> 24 - ((i - 1) * 8)) & 0xFF;
	}
	for (auto chr : lengthAsString)
	{
		msg[i] = (char)((int)chr - '0');
		i++;
	}
	for (auto chr : info.dump())
	{
		msg[i] = (char)((int)chr - '0');
		i++;
	}
	msg[i] = '\0';
	return msg;
}

const char* JsonResponsePacketSerializer::serializeResponse(GetHighScoreResponse highScore)
{
	json info;//The json that represents the information we want to return.
	int j = 0;
	std::vector<std::string>stats = highScore.statistics; //Getting the object of the statistics.
	string data = ""; //The string that represents the data in the vector.
	unsigned long n = 175;
	for (j = 0; j < stats.size(); j++)
	{
		data += stats[j]; //Adding the current stat from the vector.
		data += ",";
	}
	data[data.length() - 1] = NULL;
	info["status"] = highScore.status;
	info["statistics"] = data; //Adding data to the information json object.
	int length = info.dump().length();
	std::string lengthAsString = std::to_string(length);
	char* msg = (char*)malloc(2 + LEN_OF_DATA + length);
	int i = 0;
	msg[0] =highScore.status;
	for (int i = 1; i < (LEN_OF_DATA - lengthAsString.length() + 1); i++)
	{
		msg[i] = (n >> 24 - ((i - 1) * 8)) & 0xFF;
	}
	for (auto chr : lengthAsString)
	{
		msg[i] = (char)((int)chr - '0');
		i++;
	}
	for (auto chr : info.dump())
	{
		msg[i] = (char)((int)chr - '0');
		i++;
	}
	msg[i] = '\0';
	return msg;
}

const char* JsonResponsePacketSerializer::serializeResponse(LogoutResponse LogoutResponse)
{
	const char* msg = CreateMessage(LogoutResponse);
	return msg;
}

template<typename ResponseStruct>
const char* CreateMessage(ResponseStruct curStruct)
{
	unsigned long n = 175;
	unsigned int status = curStruct.status;
	json curJson;
	curJson["status"] = std::to_string((char)status);
	int lengthRes = curJson.dump().length(); //Getting the length of the response.
	string lengthAsString = std::to_string(lengthRes); //Converting the number to string.
	char* msg = new char[2 + LEN_OF_DATA + lengthRes]; //This is the final message we are going to return. 
	int i = 1;
	msg[0] = curStruct.status;
	
	for (int i = 1; i < (LEN_OF_DATA - lengthAsString.length()) + 1; i++)
	{
		msg[i] = (n >> 24 - ((i - 1) * 8)) & 0xFF;
	}

	for (auto chr : lengthAsString)
	{
		msg[i] = (char)((int)chr - '0');
		i++;
	}

	for (auto chr : curJson.dump())
	{
		msg[i] = chr;
		i++;
	}
	msg[i] = '\0';

	return msg;
}
