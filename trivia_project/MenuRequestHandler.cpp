#include "MenuRequestHandler.h"
#include<iostream>
#include<sstream>
#include "RequestHandlerFactory.h"
#include "Response.h"
#include "Request.h"
#define ERROR_CODE 9
#define LOGIN_CODE 2
#define SIGNUP_CODE 1
#define LOGOUT_CODE 3
#define CREATE_ROOM_CODE 4
#define JOIN_ROOM_RESPONSE_CODE 5
#define GET_ROOMS_RESPONSE_CODE 6
#define GET_PLAYERS_IN_ROOM_RESPONSE 7
#define GET_HIGH_SCORE_RESPONSE 8
#define GET_HIGH_SCORE_CODE 11
#define GET_ROOM_CODE 13
#define GET_PERSONAL_STATS_CODE 14

/*
The function creates the menu request handler object.
input: the room manager type of object, the statistics manager and the factory.
output: none.
*/
MenuRequestHandler::MenuRequestHandler( RequestHandlerFactory* handle)
{
    this->m_handlerFactor = handle;
}

/*
The function deletes all the varriables.
input: none.
output: none.
*/
MenuRequestHandler::~MenuRequestHandler()
{

}
/*
The function checks if the request is relevent.
input: the request info object.
output: true if the request is relevent and false if not relevent.
*/
bool MenuRequestHandler::isRequestRelevent(RequestInfo info)
{
    if (info.id > LOGIN_CODE && info.id < ERROR_CODE)
    {
        return true;
    }
    return false;
}

RequestResult MenuRequestHandler::handleRequest(RequestInfo info)
{
    RequestResult* result = new RequestResult; //Creating the return object.
    //First of all checking if the request information is fine.
    if (!this->isRequestRelevent(info))
    {
        result->response = nullptr;
        result->newHandler = this;
        return *result;
    }
    if (info.id == LOGOUT_CODE)
    {
        *result = this->signout(info);
    }
    else if (info.id == CREATE_ROOM_CODE)
    {
        *result = this->createRoom(info);
    }
    else if (info.id == JOIN_ROOM_RESPONSE_CODE)
    {
        *result = this->joinRoom(info);
    }
    else if (info.id == GET_ROOMS_RESPONSE_CODE)
    {
        *result = this->getRooms(info);
    }
    else if (info.id == GET_PLAYERS_IN_ROOM_RESPONSE)
    {
        *result = this->getPlayersInRoom(info);
    }
    else if (info.id == GET_HIGH_SCORE_RESPONSE)
    {
        *result = this->getHighScore(info);
    }
    else if (info.id == GET_PERSONAL_STATS_CODE)
    {
        *result = this->getPersonalStats(info);
    }
    return *result;
}
/*
The function signs out the user.
input: the request info.
output: the request result.
*/
RequestResult MenuRequestHandler::signout(RequestInfo requestInfo)
{
    RequestResult *result = new RequestResult; //The returning varriable.
    int logout_status =  JsonRequestPacketDeserializer::logoutRequest(); //Getting the status of the loging out.
    string username = JsonRequestPacketDeserializer::convertFromVectorOfCharsToString(requestInfo.buffer); //Getting the wanted user name.
    bool status = this->m_handlerFactor->logout(username); //Login out the spesific user name.
    if (status) {
        LogoutResponse ResponselogOut;
        ResponselogOut.status = 1;
        result->response = (char*)JsonResponsePacketSerializer::serializeResponse(ResponselogOut); //Getting the information.
        result->newHandler = nullptr;
    }
    result->response = nullptr;
    result->newHandler = nullptr;
    return *result;
}
/*
The function returns the rooms.
input: the request information.
output: the request result struct of information.
*/
RequestResult MenuRequestHandler::getRooms(RequestInfo requestInfo)
{
    RequestResult* result = new RequestResult; //The returning varriable.
    //std::vector<string>data = this->m_handlerFactor->getRoomManager()->getRooms(); //Getting all the rooms.
    result->newHandler = nullptr;
    GetRoomsResponse getRooms;
    //getRooms.rooms = data;
    getRooms.status = 1;
    result->response = (char*)JsonResponsePacketSerializer::serializeResponse(getRooms);
    return *result;
}
/*
The get players in room function, returns all the players in a spesific room.
input: the request info type of information.
output: the request result type of information.
*/
RequestResult MenuRequestHandler::getPlayersInRoom(RequestInfo requestInfo)
{
    RequestResult* result = new RequestResult; //The result struct itself.
    const char* requestPlayers = JsonRequestPacketDeserializer::convertFromVectorOfCharsToString(requestInfo.buffer).c_str(); //Converting the request to const char*.
    GetPlayersInRoomRequest getPlayers = JsonRequestPacketDeserializer::deserializeGetPlayersRequest(requestPlayers);
    GetPlayersInRoomResponse responseGetPlayers; //The object we are going to return.
    responseGetPlayers.players = this->m_handlerFactor->getRoomManager()->getPlayersInRoom(getPlayers.room_id); //Getting the players.
    result->newHandler = nullptr;
    result->response = (char*)JsonResponsePacketSerializer::serializeResponse(responseGetPlayers);
    return *result;
}
/*
The function returns the personal stats of a user.
input: the request information struct.
output: the request result struct.
*/
RequestResult MenuRequestHandler::getPersonalStats(RequestInfo requestInfo)
{
    RequestResult* result = new RequestResult(); //The returned varriable.
    GetPersonalStatsRequest getPersonalStatsRequest = JsonRequestPacketDeserializer::deserializeGetPersonalStatsRequest(JsonRequestPacketDeserializer::convertFromVectorOfCharsToString(requestInfo.buffer).c_str());
    GetPersonalStatsResponse response;
    response.status = 1;
    vector<string> stats = this->m_handlerFactor->getStatisticsManager()->getUserStatistics(getPersonalStatsRequest.name);
    result->response = (char*)JsonResponsePacketSerializer::serializeResponse(response);
    result->newHandler = nullptr;
    return *result;
}
/*
The function returns the high score stats.
input: the request information struct.
output: the request result struct.
*/
RequestResult MenuRequestHandler::getHighScore(RequestInfo requestInfo)
{
    RequestResult* requestResult = new RequestResult(); //The varriable we are going to return.
    int highScoreRequest = JsonRequestPacketDeserializer::getHighScoreRequest(); //Getting the high score request.
    //Doing all the process of getting the high score.
    std::vector<string>stats = this->m_handlerFactor->getStatisticsManager()->getHighScoreStats(); //Getting high score statistics.
    GetHighScoreResponse responseGetHighScore;
    responseGetHighScore.status = 1;
    responseGetHighScore.statistics = stats;
    //Creating the request result.
    requestResult->newHandler = nullptr;
    requestResult->response = (char*)JsonResponsePacketSerializer::serializeResponse(responseGetHighScore);
    return *requestResult;
}
/*
The function returns the join room request result.
input: the request info.
output: the request result type of struct.
*/
RequestResult MenuRequestHandler::joinRoom(RequestInfo requestInfo)
{
    RequestResult* result = new RequestResult(); //The returned varriable.
    const char* requestJoinRoom = JsonRequestPacketDeserializer::convertFromVectorOfCharsToString(requestInfo.buffer).c_str();//Getting the request content.
    JoinRoomRequest joinRoom = JsonRequestPacketDeserializer::deserializeJoinRoomRequest(requestJoinRoom); 
    JoinRoomResponse responseJoinRoom; //The response returnd to the client.
    if (this->m_handlerFactor->getRoomManager()->joinRoom(joinRoom.room_id, joinRoom.name) == true)
    {
        responseJoinRoom.status = 1;
        //If the process succeded.
        result->response = (char*)JsonResponsePacketSerializer::serializeResponse(responseJoinRoom);
    }
    else
    {
        responseJoinRoom.status = 0;
        //If the process succeded.
        result->response = (char*)JsonResponsePacketSerializer::serializeResponse(responseJoinRoom);
    }
    result->newHandler = nullptr;
    return *result;
}
/*
The function returns the result of the request.
input: the request information type of object.
output: the request result type of information.
*/
RequestResult MenuRequestHandler::createRoom(RequestInfo requestInfo)
{
    RequestResult* result = new RequestResult; //Defining the varriable I want to return.
    const char* request = JsonRequestPacketDeserializer::convertFromVectorOfCharsToString(requestInfo.buffer).c_str();//Getting the buffer request information.
    CreateRoomRequest createRoom = JsonRequestPacketDeserializer::deserializeCreateRoomRequest(request);
    RoomData* roomData = new RoomData; //Creating the new room data object we are going to send to the room manager.
    roomData->maxPlayers = createRoom.maxUsers;
    roomData->timePerQuestion=createRoom.answerTimeout;
    roomData->numOfQuestionsInGame = createRoom.questionCount;
    roomData->name = createRoom.roomName;
    LoggedUser* loggedUser = new LoggedUser(createRoom.roomName);
    this->m_handlerFactor->getRoomManager()->createRoom(*loggedUser,*roomData);
    CreateRoomResponse response;
    response.status = 1;
    result->newHandler = nullptr;
    result->response = (char*)JsonResponsePacketSerializer::serializeResponse(response);
    return *result;
}

