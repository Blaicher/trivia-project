#pragma once
#include<iostream>
#include<stdio.h>
#include<string>
#include "json.hpp"
using std::string;
using json = nlohmann::json;
//Different request structures.
struct LoginRequest
{
	string username;
	string password;
};
struct SignupRequest
{
	string username;
	string password;
	string email;
};
struct LeaveRoomRequest
{
	int id;
};
struct GetRoomStateRequest
{
	int id;
};
struct StartGameRequest
{
	int id;
};
struct CloseRoomRequest
{
	int id;
};
struct GetPersonalStatsRequest
{
	int id;
	string name;
};
struct CreateRoomRequest
{
	string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeout;
};
struct GetPlayersInRoomRequest
{
	unsigned int room_id;
};
struct JoinRoomRequest
{
	unsigned int room_id;
};
static class JsonRequestPacketDeserializer
{
public:
	static GetPersonalStatsRequest deserializeGetPersonalStatsRequest(const char*buffer);
	static CloseRoomRequest* deserializeCloseRoomRequest(const char* buffer);
	static StartGameRequest* deserializeStartGameRequest(const char* buffer);
	static GetRoomStateRequest* deserializeGetRoomStateRequest(const char* buffer);
	static LeaveRoomRequest* deserializeLeaveRoomRequest(const char* buffer);
	static int getHighScoreRequest();
	static int logoutRequest();
	static int getRoomRequest();
	static LoginRequest deserializeLoginRequest(const char* Buffer);
	static SignupRequest deserializeSignupRequest(const char* Buffer);
	static GetPlayersInRoomRequest deserializeGetPlayersRequest(const char* buffer);
	static JoinRoomRequest deserializeJoinRoomRequest(const char* buffer);
	static CreateRoomRequest deserializeCreateRoomRequest(const char* buffer);
	static std::string  convertFromVectorOfCharsToString(std::vector<unsigned char>vecChars);
};
