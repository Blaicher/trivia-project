#pragma once
#include<iostream>
#include<stdio.h>
#include<string>
#include "json.hpp"
#include<vector>
#include "Room.h"
#define ERROR_CODE 9
#define LOGIN_CODE 2
#define SIGNUP_CODE 1
using namespace std;

using json = nlohmann::json;
//Different response structures.
struct CloseRoomResponse
{
	int status;
};
struct StartGameResponse
{
	int status;
};
struct GetRoomStateResponse
{
	unsigned int status;
	bool hasGameBegun;
	std::vector<string>players;
	unsigned int questionCount;
	time_t answerTimeout;
};
struct LeaveRoomResponse
{
	int status;
};
struct LoginResponse
{
	unsigned int status;
};
struct SignupResponse
{
	unsigned int status;
};
struct ErrorResponse
{
	std::string message;
};
struct LogoutResponse
{
	unsigned int status;
};

struct JoinRoomResponse
{
	unsigned int status;
};
struct GetRoomsResponse
{
	unsigned int status;
	std::vector<std::string>rooms; 
};
struct GetPlayersInRoomResponse
{
	vector<string> players;
};
struct GetHighScoreResponse
{
	unsigned int status;
	std::vector<string>statistics;
};
struct GetPersonalStatsResponse
{
	unsigned int status;
	std::vector<string>statistics;
};
struct CreateRoomResponse
{
	unsigned int status;
};

static class JsonResponsePacketSerializer
{
public:
	static const char* serializeResponse(CloseRoomResponse response);
	static const char* serializeResponse(StartGameResponse response);
	static const char* serializeResponse(GetRoomStateResponse response);
	static const char* serializeResponse(LeaveRoomResponse response);
	static const char* serializeResponse(ErrorResponse ErrorResponse);
	static const char* serializeResponse(LoginResponse LoginRequest);
	static const char* serializeResponse(SignupResponse SignupResponse);
	static const char* serializeResponse(LogoutResponse logoutRes);
	static const char* serializeResponse(GetRoomsResponse getRoomsResponse);
	static const char* serializeResponse(GetPlayersInRoomResponse getPlayers);
	static const char* serializeResponse(JoinRoomResponse roomResponse);
	static const char* serializeResponse(CreateRoomResponse createRooms);
	static const char* serializeResponse(GetPersonalStatsResponse GetStatisticsResponse);
	static const char* serializeResponse(GetHighScoreResponse highScore);
};
