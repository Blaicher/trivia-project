#pragma once
#include "RoomManager.h"
#include "LoginRequestHandler.h"
#include "MenuRequestHandler.h"
#include "LoginManager.h"
#include "StatisticsManager.h"
#include "RoomMemberRequestHandler.h"
#include "RoomAdminRequestHandler.h"
using namespace std;
class RoomAdminRequestHandler;
class RoomMemberRequestHandler;
class LoginRequestHandler;
class MenuRequestHandler;
class RoomManager;
class RequestHandlerFactory
{
public:
    RequestHandlerFactory(IDataBaseAccess* dataBase);
    ~RequestHandlerFactory();
    MenuRequestHandler* createMenuRequestHandler();
    bool signUpToDB(string username,string password,string email);
    bool loginToDB(string username, string password);
    LoginRequestHandler* createLoginRequestHandler();
    LoginManager* getLoginManager();
    StatisticsManager* getStatisticsManager();
    RoomManager* getRoomManager();
    bool logout(string username);
    RoomAdminRequestHandler* createRoomAdminRequestHandler();
    RoomMemberRequestHandler* createRoomMemberRequestHandler();
private:
    LoginManager* loginManager;
    IDataBaseAccess* m_database;
    RoomManager* m_roomManager;
    StatisticsManager* m_statisticsManager;
};