#include "Request.h"
#include "Response.h"
#define USERNAME "username"
#define PASSWORD "password"
#define EMAIL "email"
#define MIN_SIZE_LOGIN 32
#define MIN_SIZE_SIGNUP 40
#define GET_HIGH_SCORE_CODE 11
#define LOGOUT_CODE 3
#define GET_ROOM_CODE 13
#define GET_PERSONAL_STATS_CODE 14
GetPersonalStatsRequest JsonRequestPacketDeserializer::deserializeGetPersonalStatsRequest(const char* buffer)
{
	string bufferAsString = buffer;
	GetPersonalStatsRequest* request = new GetPersonalStatsRequest;
	json deserialized = json::parse(bufferAsString);
	request->id = deserialized["id"];
	request->name = deserialized["name"];
	return *request;
}
/*
Deserializing the close room request to the object itself.
input: the const char of the request.
output: the object with all the data.
*/
CloseRoomRequest* JsonRequestPacketDeserializer::deserializeCloseRoomRequest(const char* buffer)
{
	string bufferAsString = buffer;
	CloseRoomRequest* closeRoomReq = new CloseRoomRequest; //The object we are going to return.
	json deserialized; //The json object we are going to use.
	deserialized = json::parse(bufferAsString);
	closeRoomReq->id = deserialized["id"];
	return closeRoomReq;
}
/*
The function returns the start game request as an object.
input: the buffer of the request.
output: the information as an object.
*/
StartGameRequest* JsonRequestPacketDeserializer::deserializeStartGameRequest(const char* buffer)
{
	string bufferAsString = buffer;
	StartGameRequest* startGameReq = new StartGameRequest; //The object we are going to return.
	json deserialized; //The json object we are going to use.
	deserialized = json::parse(bufferAsString);
	startGameReq->id = deserialized["id"];
	return startGameReq;
}
/*
The function deserializes the get room state request into an object.
input: the information as buffer.
output: the information as an object.
*/
GetRoomStateRequest* JsonRequestPacketDeserializer::deserializeGetRoomStateRequest(const char* buffer)
{
	string bufferAsString = buffer;
	GetRoomStateRequest* getRoomStateReq = new GetRoomStateRequest; //The object we are going to return.
	json deserialized; //The json object we are going to use.
	deserialized = json::parse(bufferAsString);
	getRoomStateReq->id = deserialized["id"];
	return getRoomStateReq;
}
/*
Deserializing the leave room reuqst that came as const char*.
input: const char* type of request.
output: the information as an object.
*/
LeaveRoomRequest* JsonRequestPacketDeserializer::deserializeLeaveRoomRequest(const char* buffer)
{
	string bufferAsString = buffer;
	LeaveRoomRequest* leaveRoomReq = new LeaveRoomRequest; //The object we are going to return.
	json deserialized; //The json object we are going to use.
	deserialized = json::parse(bufferAsString);
	leaveRoomReq->id = deserialized["id"];
	return leaveRoomReq;
}
/*
The function returns the code of the high score request.
input: none.
output: the code of this request.
*/
int JsonRequestPacketDeserializer::getHighScoreRequest()
{
	return GET_HIGH_SCORE_CODE;
}
/*
The function returns the code of this type of request.
input: none.
output: the code that matches the type of message.
*/
int JsonRequestPacketDeserializer::logoutRequest()
{
	return LOGOUT_CODE;
}
/*
The function returns the code of the room request object.
input: none.
output: the code of the room request.
*/
int JsonRequestPacketDeserializer::getRoomRequest()
{
	return GET_ROOM_CODE;
}
/*
Function will receive a buffer that is a json and will convert it into one and send the appropriate
details back in a LoginRequest struct

Input: a buffer containing the serialized json
Output: a LoginRequest struct containing the information from the given json
*/
LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(const char* buffer)
{
	json deserialized;
	string bufferAsString = buffer;
	deserialized = json::parse(bufferAsString);
	LoginRequest a;
	a.username = deserialized[USERNAME];
	a.password = deserialized[PASSWORD];
	return a;
}
/*
The function deserializes the sign up request it gets as a char* type of information.
input: const char* type of information.
output: sign up request type of object.
*/
SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(const char* Buffer)
{
	json deserialized;
	std::string jsonPart = Buffer;
	deserialized = json::parse(jsonPart);
	SignupRequest a;
	a.username = deserialized[USERNAME];
	a.password = deserialized[PASSWORD];
	a.email = deserialized[EMAIL];
	return a;
}
/*
The function deserializes the get players request it gets as const char* type of information.
input: const char* type of data.
output: the get players in room request.
*/
GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersRequest(const char* Buffer)
{
	json deserialize;
	std::string jsonPart = Buffer;
	deserialize = json::parse(jsonPart);
	GetPlayersInRoomRequest a;
	a.room_id = deserialize["id"];
	return a;
}
/*
The function deserializes the room request it gets as const char* type of information.
input: const char* type of data.
output: the room request.
*/
JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(const char* Buffer)
{
	json deserialize;
	std::string jsonPart = Buffer;
	deserialize = json::parse(jsonPart);
	JoinRoomRequest a;
	a.room_id = deserialize["id"];
	return a;
}
/*
The function deserializes the create room request it gets as const char* type of information.
input: const char* type of data.
output: the create room request.
*/
CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(const char* Buffer)
{
	json deserialize;
	std::string jsonPart = Buffer;
	deserialize = json::parse(jsonPart);
	CreateRoomRequest a;
	a.maxUsers = deserialize["maxUsers"];
	a.answerTimeout = deserialize["answerTimeout"];
	a.questionCount = deserialize["questionCount"];
	a.roomName = deserialize["roomName"];
	return a;
}
/*
The function gets a vector of unsigned char and converts it into a string.
input: the vector of unsigned chars.
output: the string it represents.
*/
std::string  JsonRequestPacketDeserializer::convertFromVectorOfCharsToString(std::vector<unsigned char>vecChars)
{
	std::string vecAsStr; //The vector represented as string.
	int i = 0; //Loup varriable.
	for (i = 0; i < vecChars.size(); i++)
	{
		vecAsStr += vecChars[i]; //Adding the character.
	}
	return vecAsStr;
}