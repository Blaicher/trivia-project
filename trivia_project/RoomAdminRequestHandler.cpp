#include "RoomAdminRequestHandler.h"
#include "Request.h"
#include "Response.h"
#include "Communicator.h"
#define CLOSE_ROOM_CODE 15
#define START_GAME_CODE 16
#define GET_ROOM_STATE 17
#define SUCCESSFULL 1
#define UNSUCCESSFULL -1
RoomAdminRequestHandler::RoomAdminRequestHandler(RequestHandlerFactory* handle)
{
    this->m_handleFactory = handle;
}
/*
Checking if the request is relevent to our handler.
input: request information.
output: true if okay.
*/
bool RoomAdminRequestHandler::isRequestRelevent(RequestInfo info)
{
    return info.id == CLOSE_ROOM_CODE || info.id == START_GAME_CODE || info.id == GET_ROOM_STATE;
}
/*
The function takes care of a spesific request information it got.
input: reuqest info type of object.
output: request result type of information.
*/
RequestResult RoomAdminRequestHandler::handleRequest(RequestInfo info)
{
    RequestResult* result = new RequestResult; //The object we are going to return.
    //First of all checking if the request is legel.
    if (this->isRequestRelevent(info) == false)
    {
        result->newHandler = this->m_handleFactory->createRoomAdminRequestHandler();
        result->response = nullptr;
    }
    else
    {
        if (info.id == CLOSE_ROOM_CODE)
        {
            *result = this->closeRoom(info); //Getting the result.
        }
        else if (info.id == START_GAME_CODE)
        {
            *result = this->startGame(info); //Getting the result.
        }
        else
        {
            *result = this->getRoomState(info); //Getting the result.
        }
    }
    return *result;
}
/*
The function takes care of the closing room process.
input: the request's information.
output: the result of the request.
*/
RequestResult RoomAdminRequestHandler::closeRoom(RequestInfo info)
{
    int i = 0; //Loup varriable.
    CloseRoomRequest* closeRoomReq = JsonRequestPacketDeserializer::deserializeCloseRoomRequest(JsonRequestPacketDeserializer::convertFromVectorOfCharsToString(info.buffer).c_str()); //Getting the right object.
    std::vector<LoggedUser>users = this->m_room->getM_user(); //Getting the users.
    for ( i = 0; i < users.size(); i++)
    {
        this->m_room->removeUser(users[i]); //Deleting each and each user.
    }
    this->m_handleFactory->getRoomManager()->deleteRoom(closeRoomReq->id); //Calling the function of closing a room.
    CloseRoomResponse* response = new CloseRoomResponse;
    response->status = closeRoomReq->id; //The process was succesfull.
    RequestResult* result = new RequestResult; //The object we are going to return.
    result->response = (char*)JsonResponsePacketSerializer::serializeResponse(*response); //Getting the response.
    LeaveRoomResponse responseLeaveRoom;
    responseLeaveRoom.status = 1;
    Communicator::sendToMultipleUsers(this->m_room->getAllUsers(),JsonResponsePacketSerializer::serializeResponse(responseLeaveRoom));
    return *result;
}
/*
The function starts a spesific game.
input: the request information.
output: the request result.
*/
RequestResult RoomAdminRequestHandler::startGame(RequestInfo info)
{
    int i = 0; //Loup varriable.
    StartGameRequest* request = JsonRequestPacketDeserializer::deserializeStartGameRequest(JsonRequestPacketDeserializer::convertFromVectorOfCharsToString(info.buffer).c_str());
    StartGameResponse *response = new StartGameResponse;
    response->status = request->id;
    RequestResult* result = new RequestResult; 
    result->newHandler = nullptr;
    result->response = (char*)JsonResponsePacketSerializer::serializeResponse(*response);
    Communicator::sendToMultipleUsers(this->m_room->getAllUsers(), JsonResponsePacketSerializer::serializeResponse(*response));
    return *result;
}
/*
The function returns a spesific room state.
input: the information about a spesific room.
output: the request result.
*/
RequestResult RoomAdminRequestHandler::getRoomState(RequestInfo info)
{
    GetRoomStateRequest* roomRequest = JsonRequestPacketDeserializer::deserializeGetRoomStateRequest(JsonRequestPacketDeserializer::convertFromVectorOfCharsToString(info.buffer).c_str()); //Getting the right object. 
    unsigned int status = this->m_handleFactory->getRoomManager()->getRoomState(roomRequest->id); //Getting the id of the object.    
    GetRoomStateResponse *response = new GetRoomStateResponse;
    response->status = status;
    response->answerTimeout = info.receivalTime;
    if (status == 1)
    {
        response->hasGameBegun = true;
    }
    response->players = this->m_handleFactory->getRoomManager()->getPlayersInRoom(roomRequest->id);
    response->questionCount = 10;
    RequestResult* result = new RequestResult;
    result->newHandler = nullptr;
    result->response = (char*)JsonResponsePacketSerializer::serializeResponse(*response);
    return *result;
}
