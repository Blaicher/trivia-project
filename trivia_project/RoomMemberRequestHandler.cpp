#include "RoomMemberRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "Request.h"
#include "Response.h"
#include "Communicator.h"
#define CLOSE_ROOM_CODE 15
#define START_GAME_CODE 16
#define GET_ROOM_STATE 17
#define LEAVE_ROOM 18
#define LEAVE_GAME 19
/*
The function is the constructor of the class.
input: the factory to use.
output: none.
*/
RoomMemberRequestHandler::RoomMemberRequestHandler(RequestHandlerFactory* handler)
{
	this->m_handleFactory = handler;
}

/*
The function leaves the room.
input: the request information.
output: the request result.
*/
RequestResult RoomMemberRequestHandler::leaveRoom(RequestInfo info)
{
	LeaveRoomRequest* leave = JsonRequestPacketDeserializer::deserializeLeaveRoomRequest(JsonRequestPacketDeserializer::convertFromVectorOfCharsToString(info.buffer).c_str()); //Getting the object.
	RequestResult* result = new RequestResult; //The returned object.
	LeaveRoomResponse* response = new LeaveRoomResponse;
	response->status = leave->id;
	result->newHandler = nullptr;
	result->response = (char*)JsonResponsePacketSerializer::serializeResponse(*response);
	string leaveRoom = "Leaving the room!!!";
	Communicator::sendToMultipleUsers(this->m_room->getAllUsers(),leaveRoom.c_str());
	return *result;	
}
/*
The function starts the game.
input: the request information.
output: the request result.
*/
RequestResult RoomMemberRequestHandler::startGame(RequestInfo info)
{
	StartGameRequest* request = JsonRequestPacketDeserializer::deserializeStartGameRequest(JsonRequestPacketDeserializer::convertFromVectorOfCharsToString(info.buffer).c_str());
	StartGameResponse* response = new StartGameResponse;
	response->status = request->id;
	RequestResult* result = new RequestResult;
	result->newHandler = nullptr;
	result->response = (char*)JsonResponsePacketSerializer::serializeResponse(*response);
	string startGame = "Starting the game!!!";
	Communicator::sendToMultipleUsers(this->m_room->getAllUsers(),startGame.c_str());
	return *result;
}
/*
The function returns the room's state.
input: request's information.
output: the result of the room state's request.
*/
RequestResult RoomMemberRequestHandler::getRoomState(RequestInfo info)
{
	GetRoomStateRequest* roomRequest = JsonRequestPacketDeserializer::deserializeGetRoomStateRequest(JsonRequestPacketDeserializer::convertFromVectorOfCharsToString(info.buffer).c_str()); //Getting the right object. 
	unsigned int status = this->m_handleFactory->getRoomManager()->getRoomState(roomRequest->id); //Getting the id of the object.    
	GetRoomStateResponse* response = new GetRoomStateResponse;
	response->status = status;
	response->answerTimeout = info.receivalTime;
	if (status == 1)
	{
		response->hasGameBegun = true;
	}
	response->players = this->m_handleFactory->getRoomManager()->getPlayersInRoom(roomRequest->id);
	response->questionCount = 10;
	RequestResult* result = new RequestResult;
	result->newHandler = nullptr;
	result->response = (char*)JsonResponsePacketSerializer::serializeResponse(*response);
	return *result;
}
/*
The function returns true if the request is relevent to this handler.
input: the request information.
output: the information about the request.
*/
bool RoomMemberRequestHandler::isRequestRelevent(RequestInfo info)
{
	return info.id == LEAVE_GAME|| info.id == START_GAME_CODE || info.id == LEAVE_ROOM;
}
/*
The function handlers a request object.
input: the request's information.
output: request result object.
*/
RequestResult RoomMemberRequestHandler::handleRequest(RequestInfo info)
{
	RequestResult* result = new RequestResult; //The object we are going to return.
	if (this->isRequestRelevent(info) == false)
	{
		result->response = nullptr;
		result->newHandler = this->m_handleFactory->createRoomMemberRequestHandler();
	}
	else
	{
		if (info.id == LEAVE_ROOM)
		{
			*result = this->leaveRoom(info);
		}
		else
		{
			*result = this->startGame(info);
		}
	}
	return *result;
}
