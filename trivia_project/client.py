import socket
import binascii
HOST = '127.0.0.1'  # Standard loopback interface address (localhost)
PORT = 9999        # Port to listen on (non-privileged ports are > 1023)

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client:
    try:
        client.connect((HOST, PORT)) # Connecting to server address
        json_string = "{id:1}"
        msg = "16"+str(len(json_string))+json_string
        client.sendall(msg.encode())
    except ConnectionError:
        print("Error connecting to server!")
    except ConnectionAbortedError:
        print("Error: Server ended the connection")
    except ConnectionRefusedError:
        print("Error while connecting to server")
