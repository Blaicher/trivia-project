#include "LoginManager.h"
#include "IDataBaseAccess.h"

/*
The constructor of the class, building all the objects needed.
input: none.
output: none.
*/
LoginManager::LoginManager(IDataBaseAccess* sqlite)
{
	m_database = sqlite; //Creating the new database.
	//This is the four loup which is going to create the vector.
	std::unordered_map<std::string, std::string>users = ((SqliteDataBase*)m_database)->getUsers();//Calling to the get users function.
	std::unordered_map<std::string, std::string>::iterator itVectorUsers; //The iterator passing all the users.
	itVectorUsers = users.begin();
	for (; itVectorUsers!=users.end(); itVectorUsers++)
	{
		//Passing all the players.
		LoggedUser newLoggedUser((*itVectorUsers).first); //Creating the logged user to add to the vector.
		this->m_loggedUsers.push_back(&newLoggedUser); //Adding the logged user object.
	}
}
/*
The destructor of the class, deleting all the objects and free their memory.
input: none.
output: none.
*/
LoginManager::~LoginManager()
{
	this->m_loggedUsers.clear();
	delete m_database;
}
/*
The function signs up a new user upon the parameters it gets.
input: user name,password and email.
output: true if succeded and false if not.
*/
bool LoginManager::signUp(string name, string password, string email)
{
	return ((SqliteDataBase*)this->m_database)->signUp(name,password,email);
}
/*
The function does the login proccess.
input: the strings of name and password.
output: true if everything is fine and false if not.
*/
bool LoginManager::login(string name, string password)
{
	return ((SqliteDataBase*)this->m_database)->checkPassword(name,password);
}
/*
The function logs a user out.
input: user name string.
output: true if succeded and false if failed.
*/
bool LoginManager::logout(string username)
{
	return ((SqliteDataBase*)this->m_database)->logout(username);
}
