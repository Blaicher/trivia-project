#pragma comment (lib, "ws2_32.lib")
#include "Response.h"
#include "Request.h"
#include "WSAInitializer.h"
#include "Server.h"

#include <iostream>
#include <exception>
using std::cout;
int main()
{
	try
	{
		WSAInitializer wsaInit;
		Server myServer;

		myServer.run();
	}
	catch (std::exception& e)
	{
		std::cout << "Error occured: " << e.what() << std::endl;
	}
	system("PAUSE");
	return 0;
}