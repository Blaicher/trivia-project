#pragma once
#include "IRequestHandler.h"
#include "Room.h"
#include "LoggedUser.h"
#include "Communicator.h"
class RoomMemberRequestHandler :public IRequestHandler
{
public:
	RoomMemberRequestHandler(RequestHandlerFactory*handler);
	RequestResult leaveRoom(RequestInfo info);
	RequestResult startGame(RequestInfo info);
	RequestResult getRoomState(RequestInfo info);
	bool isRequestRelevent(RequestInfo info);
	RequestResult handleRequest(RequestInfo info);
private:
	Room *m_room;
	LoggedUser* m_user;
	RoomManager* m_roomManager;
	RequestHandlerFactory* m_handleFactory;
};