#include "LoginRequestHandler.h"
#include<iostream>
#include<vector>
#include "Response.h"
#include "Request.h"
#include<sstream>
#include "MenuRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "Communicator.h"
#define ERROR_CODE 9
#define LOGIN_CODE 2
#define SIGNUP_CODE 1
#define SIZE 50
//Global functions that might help us:
LoginRequestHandler::LoginRequestHandler(RequestHandlerFactory* factory)
{
	this->m_factory = factory;
}
/*
The function checks if the request info object it gets is relvent to
the spesific type.
input: request info type of object.
output: true if relevent and false if not.
*/
bool LoginRequestHandler::isRequestRelevent(RequestInfo info)
{
	return info.id == LOGIN_CODE || info.id == SIGNUP_CODE; 
}
/*
The function gets the request information about the user and connects 
the user to the databse using the login manager.
input: the information about the user.
output: the request result type of object that describes the response to that user's information.
*/
RequestResult LoginRequestHandler::handleRequest(RequestInfo info)
{
	//The possibilites of the requests objects:
	LoginRequest loginReq;
	SignupRequest signUpReq;
	//The possibilities of the respons objects:
	SignupResponse signUpRes; 
	LoginResponse loginResponse;
	RequestResult result; //The result request struct we are going to return.
	//Checking if it's not the right handle request.
	if (this->isRequestRelevent(info) == false)
	{
		loginResponse.status = 2;
		result.response = (char*)JsonResponsePacketSerializer::serializeResponse(loginResponse);
		result.newHandler = this->m_factory->createLoginRequestHandler();
		return result;
	}
	//Checking what kind of information we have.
	if (info.id == LOGIN_CODE)
	{
		loginReq = JsonRequestPacketDeserializer::deserializeLoginRequest(JsonRequestPacketDeserializer::convertFromVectorOfCharsToString(info.buffer).c_str()); //Sending to the function.
		bool status = this->m_factory->loginToDB(loginReq.username,loginReq.password); //Using the factory handler to login to the server.
        //Checking if something went wrong.
		if (status)
		{
			loginResponse.status = 1;
			result.response = (char*)JsonResponsePacketSerializer::serializeResponse(loginResponse);
			result.newHandler = this->m_factory->createMenuRequestHandler();
			Communicator::connectedUSers.insert(std::pair<string,SOCKET>(loginReq.username,-1));
		}
		else
		{
			loginResponse.status = 2;
			result.response = (char*)JsonResponsePacketSerializer::serializeResponse(loginResponse);
			result.newHandler = this->m_factory->createLoginRequestHandler();
			return result;
		}
	}
	else if (info.id == SIGNUP_CODE)
	{
		//moving the convert to the deserializer.
		signUpReq = JsonRequestPacketDeserializer::deserializeSignupRequest(JsonRequestPacketDeserializer::convertFromVectorOfCharsToString(info.buffer).c_str()); //Sending to the function.
		bool status = this->m_factory->signUpToDB(signUpReq.username,signUpReq.password,signUpReq.email); //Signing up to the server using the factory handler.
		//Checking if something went wrong.
		if (status)
		{
			signUpRes.status = 1;
			result.response = (char*)JsonResponsePacketSerializer::serializeResponse(signUpRes);
			Communicator::connectedUSers.insert(std::pair<string, SOCKET>(loginReq.username, -1));
		}
		else
		{
			loginResponse.status = 2;
			result.response = (char*)JsonResponsePacketSerializer::serializeResponse(loginResponse);
			result.newHandler = this->m_factory->createLoginRequestHandler();
			return result;
		}
	}
	result.newHandler = this->m_factory->createMenuRequestHandler(); //Preparing the next handler already here.
	return result;
}

