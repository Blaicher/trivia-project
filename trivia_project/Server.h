#pragma once
#include <WinSock2.h>
#include <Windows.h>
#include <thread>
#include <queue>
#include "IDataBaseAccess.h"
#include "Communicator.h"
#include "RequestHandlerFactory.h"
#define PATH_DB_DEFAULUT "trivia_data_base.sqlite"
class Server
{
public:
	Server();
	~Server();
	void run();

private:
	Communicator* m_communicator;
	IDataBaseAccess* m_database;
	RequestHandlerFactory* m_factory;
};


