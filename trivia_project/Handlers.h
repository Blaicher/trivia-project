#pragma once
#include "IRequestHandler.h"
#include <iostream>
#include <ctime>
#include <vector>
class IRequestHandler;
struct RequestResult
{
	char* response;
	IRequestHandler* newHandler;
};

struct RequestInfo
{
	int id; 
	time_t receivalTime;
	std::vector<unsigned char> buffer;
};