#pragma once
#include "IRequestHandler.h"
#include "LoginManager.h"
#include "Handlers.h"
#include<vector>
#include "RequestHandlerFactory.h"
class RequestHandlerFactory;
class LoginRequestHandler : public IRequestHandler
{
public:
	LoginRequestHandler(RequestHandlerFactory*factory);
	bool isRequestRelevent(RequestInfo info);
	RequestResult handleRequest(RequestInfo info);
private:
	RequestHandlerFactory* m_factory;
};
