#pragma once
#include<iostream>
#include<vector>
#include "LoggedUser.h"
#include "RoomData.h"
using namespace std;
class Room
{
public:
	Room();
	Room(RoomData roomData);
	~Room();
	void addUser(LoggedUser user);
	void removeUser(LoggedUser user);
	vector<string> getAllUsers();
	RoomData getRoomData();
	std::vector<LoggedUser>getM_user();
private:
	RoomData m_metadata;
	std::vector<LoggedUser>m_users;
};