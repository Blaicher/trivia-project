#include "RoomManager.h"
string getStringRoomData(RoomData room_data);
/*
The constructor of the class.
input: none.
output: none.
*/
RoomManager::RoomManager()
{
}
/*
The function deletes all the memory of the class.
input: none.
output: none.
*/
RoomManager::~RoomManager()
{
	m_rooms.clear();
}
/*
The function adding the room to the vector.
input: the logged user and the room data.
output: none.
*/
void RoomManager::createRoom(LoggedUser loggedUser, RoomData roomData)
{
	Room newRoom(roomData); //Creating the new room.
	m_rooms[counterRooms] = newRoom; //Adding a room to the vector.
	counterRooms++; //Upgrading the rooms counter.
}
/*
The function deletes the room by its id.
input: the id.
output: none.
*/
void RoomManager::deleteRoom(int id)
{
	std::map<int, Room>::iterator itRooms; //The iterator of rooms.
	for(itRooms = this->m_rooms.begin(); itRooms!=this->m_rooms.end(); itRooms++)
	{
		//Checking if the iterator if the one we are looking for.
		if ((*itRooms).first == id)
		{
			this->m_rooms.erase(itRooms); //Erasing the unneeded place.
			return;
		}
	}
}
/*
The function returns the room state.
input: the room id.
output: the room state.
*/
unsigned int RoomManager::getRoomState(int id)
{
	std::map<int, Room>::iterator itArr; //Passing the array.
	itArr = m_rooms.begin();
	for (;itArr!=this->m_rooms.end(); itArr++)
	{
		if ((*itArr).first == id)
		{
			RoomData roomData = (*itArr).second.getRoomData();
			unsigned int num = roomData.isActive; //Getting the room state.
			return num;
		}
	}
	return -1;
}
/*
The function returns a vector of room data.
input: none.
output: the vector of room data string type of information.
*/
vector<string> RoomManager::getRooms()
{
	std::vector<string>info; //The vector of room data objects.
	int i = 0; //Loup varriable.
	for ( i = 0; i < this->m_rooms.size(); i++)
	{
		info.push_back(getStringRoomData(this->m_rooms[i].getRoomData())); //Adding to the vector.
	}
	return info;
}
/*
The function joins the room by the id.
input: the id and the username.
output: true if succeded and false if not.
*/
bool RoomManager::joinRoom(int id,string username)
{
	LoggedUser* newUser = new LoggedUser(username); //The user we are going to add.
	std::map<int, Room>::iterator itRooms; //Passing all our rooms using this function.
	itRooms = this->m_rooms.begin();
	for (;  itRooms != this->m_rooms.end(); itRooms++)
	{
		//Passing all the map.
		if ((*itRooms).first == id)
		{
			(*itRooms).second.addUser(*newUser);
			return true;
		}
	}
	return false;
}
/*
The function returns the names in the vector by the id.
input: the id of the room.
output: the vector of names from type of string.
*/
vector<string> RoomManager::getPlayersInRoom(int id)
{
	std::vector<string>ansPlayers; //The vector of player we are going to return.
	std::map<int, Room>::iterator itRooms; //The iterator of rooms.
	itRooms = this->m_rooms.begin(); //Getting the reference to the first one.
	for ( ;  itRooms != this->m_rooms.end(); itRooms++)
	{
		//Passing all the map.
		if ((*itRooms).first == id)
		{
			ansPlayers = (*itRooms).second.getAllUsers(); //Getting the players.
		}
	}
	return ansPlayers;
}
/*
The function gets the room data struct and returns all the information
as a string.
input: room data object.
output: data as a string.
*/
string getStringRoomData(RoomData room_data)
{
	string data = ""; //The returned object.
	data += room_data.name + ",";
	data += to_string(room_data.id) + ",";
	data += to_string(room_data.isActive) + ",";
	data += to_string(room_data.maxPlayers) + ",";
	data += to_string(room_data.numOfQuestionsInGame) + ",";
	data += to_string(room_data.timePerQuestion) + ".";
	return data;
}
