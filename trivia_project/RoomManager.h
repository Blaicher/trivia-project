#pragma once
#include<iostream>
#include<string>
#include "RoomData.h"
#include"Room.h"
#include "Handlers.h"
#include "LoggedUser.h"
#include "Handlers.h"
#include<vector>
#include<map>
//Different defines for the states of the rooms:
#define WAITING 0
#define PLAYING 1
#define ROOM_NOT_FOUND -1
struct RoomData;
class RoomManager
{
public:
	RoomManager();
	~RoomManager();
	void createRoom(LoggedUser loggedUser,RoomData roomData);
	void deleteRoom(int id);
	unsigned int getRoomState(int id);
	std::vector<string> getRooms();  
	bool joinRoom(int id,string username);
	std::vector<string>getPlayersInRoom(int id);
	int counterRooms = 0;
private:
	std::map<int, Room> m_rooms;
};